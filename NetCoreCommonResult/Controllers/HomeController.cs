﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCoreCommonResult.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        /// <summary>
        /// string
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string Index() => "Welecome to .NetCore";

        /// <summary>
        /// 跳转，不处理
        /// </summary>
        /// <returns></returns>
        [HttpGet("redirect")]
        public ActionResult Redirect() => RedirectToAction("Index");

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("num")]
        public int Num() => 10;

        /// <summary>
        /// 异步
        /// </summary>
        /// <returns></returns>
        [HttpGet("async")]
        public Task<IEnumerable<string>> TaskString() =>Task.FromResult<IEnumerable<string>>(new[] { "A","B","C"});
        /// <summary>
        /// 文件输出，不处理
        /// </summary>
        /// <returns></returns>
        [HttpGet("file")]
        public ActionResult GetFile() => File(Encoding.UTF8.GetBytes("File String"), "text/plain");

        /// <summary>
        /// 空返回值，不处理
        /// </summary>
        /// <returns></returns>
        [HttpGet("empty")]
        public ActionResult Empty() => Empty();
        /// <summary>
        /// contentResult 不处理
        /// </summary>
        /// <returns></returns>
        [HttpGet("content")]
        public ActionResult Content() => Content("this is content");
        /// <summary>
        /// 异常，返回500错误
        /// </summary>
        /// <returns></returns>
        [HttpGet("exception")]
        public ActionResult GetException() => throw new InvalidOperationException("invalid");
        /// <summary>
        /// 自定义异常，返回200
        /// </summary>
        /// <returns></returns>
        [HttpGet("bizException")]
        public ActionResult GetBizException() => throw new Exceptions.BizException("bizException");

    }
}
