﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace NetCoreCommonResult.Filters
{
    public class CommonResultFilterAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext context)
        {
            if (context.Result is ObjectResult objRst)
            {
                if (objRst.Value is Models.ApiResult)
                    return;
                context.Result = new ObjectResult(new Models.ApiResult
                {
                    Success = true,
                    Message = string.Empty,
                    Data = objRst.Value
                });
            }
        }
    }
}
